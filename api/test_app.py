import pytest
from fastapi.testclient import TestClient
from server.app import app
from server.models.user import User
from server.models.item import Item
from server.database import session
from server.config import settings

user_Mrs_Hudson = {
    "login": "Mrs_Hudson", 
    "password": "finedine"
}

item1 = {
    "name": "item_name1"
}

item2 = {
    "name": "item_name2"
}

def truncate_DB():
    session.query(Item).delete()
    session.query(User).delete()
    session.commit()

@pytest.fixture
def client():
    yield TestClient(app)

# Чистим тестовую базу во избежание конфликтов по данным
def test_trunc1_db(client):
    truncate_DB()

def test_index(client):
    response = client.get("/")
    assert response.status_code == 200

def test_register(client):
    # Первое создание пользователя
    response = client.post("/registration", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    print(response.json())
    assert response.status_code == 200
    assert response.json()["code"] == 200
    assert response.json()["data"][0]["login"] == user_Mrs_Hudson["login"]
    assert response.json()["data"][0]["password"] == user_Mrs_Hudson["password"]

def test_register_dbl(client):
    # Повторное создание должно корректно обработаться 
    # и вернуть в ответе ошибку
    response = client.post("/registration", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 500

def test_login(client):
    # Авторизуемся
    response = client.post("/login", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 200
    assert response.json()["token"] != None

def test_login_wrong(client):
    # Авторизация с неверными данными должно корректно обработаться 
    # и вернуть в ответе ошибку
    wrong_user = {}
    wrong_user["login"] = user_Mrs_Hudson["login"]
    wrong_user["password"] = "wrong_pass"
    response = client.post("/login", json=wrong_user, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 401

def test_new_item(client):
    # Авторизуемся
    response_t = client.post("/login", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    token = response_t.json()["token"]
    # Создание итема
    response = client.post("/items/new", json=item1, headers = {"Authorization":token}, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 200
    assert response.json()["data"][0]["name"] == item1["name"]
    # Повторное создание того же итема должно корректно обработаться 
    # и вернуть в ответе ошибку
    response = client.post("/items/new", json=item1, headers = {"Authorization":token}, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 500

def test_get_items(client):
    # Авторизуемся
    response_t = client.post("/login", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    token = response_t.json()["token"]
    # Получаем список итемов
    response = client.get("/items", json=item1, \
                    headers = {"Authorization":token}, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["items"][0]["name"] == item1["name"]

def test_del_wrong_item(client):
    # Авторизуемся
    response_t = client.post("/login", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    token = response_t.json()["token"]
    # Получаем список итемов
    response_g = client.get("/items", json=item1, \
                    headers = {"Authorization":token}, \
                    allow_redirects = True)
    item_id = response_g.json()["items"][0]["_id"]
    # Удаляем итем с неверным ИД
    response = client.post(f"/items/{item_id}123123123", headers = {"Authorization":token}, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 409

def test_item_del(client):
    # Авторизуемся
    response_t = client.post("/login", json=user_Mrs_Hudson, \
                    allow_redirects = True)
    token = response_t.json()["token"]
    # Получаем список итемов
    response_g = client.get("/items", json=item1, \
                    headers = {"Authorization":token}, \
                    allow_redirects = True)
    item_id = response_g.json()["items"][0]["_id"]
    # Удаляем итем
    response = client.post(f"/items/{item_id}", headers = {"Authorization":token}, \
                    allow_redirects = True)
    assert response.status_code == 200
    assert response.json()["code"] == 200

# Убираем за собой мусор во избежание конфликтов со следующим разработчиком
def test_trunc2_db(client):
    truncate_DB()

if __name__ == "__main__":
    pytest.main()
