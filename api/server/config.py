from pydantic import BaseSettings
import os
import dotenv

dotenv.load_dotenv()

app_env = os.getenv("APP_ENV", None)

class Settings(BaseSettings):
    APP_ENV: str = ""
    PG_PASSWORD: str
    PG_USER: str
    PG_DB: str
    PG_PASSWORD_TEST: str
    PG_USER_TEST: str
    PG_DB_TEST: str
    PG_HOST: str
    PG_PORT: str
    TOKEN_KEY: str
    TOKEN_LT: int
    PG_URI: str = ""
    SENTRY_URL: str

settings = Settings(_env_file=f"{app_env}.env")
settings.APP_ENV = app_env
if app_env == "test":
    settings.PG_DB = settings.PG_DB_TEST
    settings.PG_PASSWORD = settings.PG_PASSWORD_TEST
    settings.PG_USER = settings.PG_USER_TEST
    settings.SENTRY_URL = ""
settings.PG_URI = f"postgresql://{settings.PG_USER}:{settings.PG_PASSWORD}@{settings.PG_HOST}:{settings.PG_PORT}/{settings.PG_DB}"