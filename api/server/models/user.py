from pydantic import BaseModel, Field
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from server.models.common import DeclarativeBase

class UserSchema(BaseModel):
    login: str = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "login": "test_login",
                "password": "test_password",
            }
        }

class User(DeclarativeBase):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    login = Column("login", String, unique=True)
    password = Column("password", String)
    items = relationship("Item", back_populates="user")

    def __repr__(self):
        return "".format(self.login)

def user_helper(user) -> dict:
    return {
        "_id": str(user.id),
        "login": user.login,
        "password": user.password,
    }

def ResponseModel(data, message: str) -> dict:
    return {"data": [data], "code": 200, "message": message}


def ErrorResponseModel(error: str, code: int, message: str) -> dict:
    return {"error": error, "code": code, "message": message}