from pydantic import BaseModel, Field
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from server.models.common import DeclarativeBase

class ItemSchema(BaseModel):
    name: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "name": "item_name",
            }
        }

class Item(DeclarativeBase):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    name = Column("name", String, unique=True)
    user = relationship("User", back_populates="items")

    def __repr__(self):
        return "".format(self.name)

def item_helper(item) -> dict:
    return {
        "_id": str(item.id),
        "name": item.name,
    }

def ResponseModel(data, message: str) -> dict:
    return {"data": [data], "code": 200, "message": message}


def ErrorResponseModel(error: str, code: int, message: str) -> dict:
    return {"error": error, "code": code, "message": message}

def MessageResponseModel(code: int, message: str) -> dict:
    return {"code": code, "message": message}
