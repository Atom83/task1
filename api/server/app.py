from server.config import settings
import sentry_sdk

if settings.SENTRY_URL != "":
    sentry_sdk.init(
        settings.SENTRY_URL,

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0
    )

from fastapi import FastAPI, Depends, Request
from server.routes.user import router as UserRouter
from server.routes.login import router as LoginRouter, verify_token
from server.routes.item import router as ItemRouter
from server.routes.send import router as SendRouter
from server.routes.get import router as GetRouter
import socket

app = FastAPI()
app.include_router(UserRouter, tags=["User"], prefix="/registration")
app.include_router(LoginRouter, tags=["Login"], prefix="/login")
app.include_router(ItemRouter, tags=["Item"], prefix="/items", dependencies=[Depends(verify_token)])
app.include_router(SendRouter, tags=["Send"], prefix="/send", dependencies=[Depends(verify_token)])
app.include_router(GetRouter, tags=["Get"], prefix="/get", dependencies=[Depends(verify_token)])

@app.get("/", tags=["Root"])
async def root():
    return {"message": f"Welcome to {socket.gethostbyname(socket.gethostname())}!"}
