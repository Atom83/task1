from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from server.models.common import DeclarativeBase
from server.models.user import UserSchema, User, user_helper
from server.models.item import ItemSchema, Item, item_helper
from server.config import settings

engine = create_engine(settings.PG_URI)
DeclarativeBase.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

async def add_user_db(user: UserSchema) -> dict:
    new_usr = User(login=user["login"], password=user["password"])
    session.add(new_usr)
    session.commit()
    return user_helper(new_usr)

async def get_user_db(login: str) -> dict:
    user = session.query(User).filter(User.login == login).first()
    if user != None:
        return user_helper(user)
    else:
        return None

async def add_item_db(item: ItemSchema) -> dict:
    new_item = Item(name=item["name"], user_id=item["user_id"])
    session.add(new_item)
    session.commit()
    return item_helper(new_item)

async def get_item_db(name: str, user_id: int) -> dict:
    item = session.query(Item).filter(Item.name == name, Item.user_id == user_id).first()
    if item != None:
        return item_helper(item)
    else:
        return None

async def get_item_by_id_db(item_id: str) -> dict:
    item = session.query(Item).filter(Item.id == item_id).first()
    if item != None:
        return item_helper(item)
    else:
        return None

async def del_item_db(user_id: int, item_id: int) -> str:
    cnt = session.query(Item).filter(Item.id == item_id, Item.user_id == user_id)\
        .delete(synchronize_session="fetch")
    session.commit()
    return cnt

async def get_items_db(user_id: int) -> dict:
    items = session.query(Item).filter(Item.user_id == user_id).all()
    item_lst = []
    if items != None:
        for item in items:
            item_lst.append(item_helper(item))
        return {"items": item_lst}
    else:
        return None

async def transfer_item_db(user_id: str, item_id: int) -> dict:
    item_cnt = session.query(Item).filter(Item.id == item_id)\
                      .update({"user_id": user_id})
    session.commit()
    return item_cnt
