from server.app import sentry_sdk
from fastapi import APIRouter, Body, Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.exc import SQLAlchemyError
from server.database import (add_item_db, get_item_db, del_item_db
                           , get_user_db, get_items_db, session)
from server.models.item import (ItemSchema, ResponseModel
                              , ErrorResponseModel)
from server.routes.login import verify_token

router = APIRouter()

@router.post("/new", response_description="Item added into db")
async def add_item(item: ItemSchema = Body(...)):
    item = jsonable_encoder(item)
    user = await get_user_db(verify_token.loads["login"])
    item["user_id"] = user["_id"]
    try:
        new_item = await add_item_db(item)
        sentry_sdk.capture_message(f"Item added into db")
        return ResponseModel(new_item, "Item was added successfully")
    except SQLAlchemyError as e: 
        session.rollback()
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Internal server error", 500, e.orig.args)
    except Exception as e: 
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Error", 520, e)

@router.post("/{item_id}", response_description="Item deleted from db")
async def del_item(item_id: int):
    user = await get_user_db(verify_token.loads["login"])
    if user["_id"] == None:
        sentry_sdk.capture_message(f"Unknown user tried to delete some items. Ah ah ah.")
        return ErrorResponseModel("Not found", 404, "User not found")
    del_res = await del_item_db(user["_id"], item_id)
    if del_res == 0:
        sentry_sdk.capture_message(f"Trying to delete non-existential item :o)")
        return ErrorResponseModel("Data conflict", 409, "No items to delete")
    elif del_res > 0:
        sentry_sdk.capture_message(f"Item deleted")
        return ErrorResponseModel("Data ok", 200, "Item deleted")
    else:
        sentry_sdk.capture_message(f"Something strange going on, something wrong")
        return ErrorResponseModel("Unknown error", 520, "Something strange going on, something wrong")

@router.get("/", response_description="Items received from db")
async def get_items():
    user = await get_user_db(verify_token.loads["login"])
    if user["_id"] == None:
        sentry_sdk.capture_message(f"Oops you'v got no items")
        return ErrorResponseModel("Not found", 404, "User not found")
    items = await get_items_db(user["_id"])
    sentry_sdk.capture_message(f"Got some items for you.")
    return items