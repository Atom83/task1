from server.app import sentry_sdk
from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder
from server.database import add_user_db, session
from server.models.user import (UserSchema, ResponseModel, ErrorResponseModel)
from sqlalchemy.exc import SQLAlchemyError

router = APIRouter()

@router.post("/", response_description="User added into db")
async def add_user(user: UserSchema = Body(...)):
    user = jsonable_encoder(user)
    try:
        new_user = await add_user_db(user)
        sentry_sdk.capture_message(f"User {new_user['login']} was added successfully")
        return ResponseModel(new_user, "User was added successfully")
    except SQLAlchemyError as e: 
        session.rollback()
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Internal server error", 500, e.orig.args)
    except Exception as e: 
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Error", 520, e)
