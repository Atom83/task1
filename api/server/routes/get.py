from server.app import sentry_sdk
from fastapi import APIRouter, Body, Request
from itsdangerous import SignatureExpired, BadSignature
from server.routes.login import verify_token
from server.database import transfer_item_db
from server.models.item import ErrorResponseModel

router = APIRouter()

@router.get("", response_description="Item transfer completed")
async def get(request: Request):
    params = request.query_params
    token = params.get('act')
    verify_token(request, token)
    item_id = verify_token.loads["item_id"]
    user_id = verify_token.loads["user_id"]
    item_cnt = await transfer_item_db(user_id, item_id)
    if item_cnt == 0:
        sentry_sdk.capture_message(f"Unknown user wants some items to get")
        return ErrorResponseModel("Not found", 404, "User not found")
    elif item_cnt == None:
        sentry_sdk.capture_message(f"Something strange going on, something wrong")
        return ErrorResponseModel("Unknown error", 520, "Something strange going on, something wrong")
    else:
        sentry_sdk.capture_message(f"Item transferred")
        return ErrorResponseModel("Data ok", 200, "Item transferred")
