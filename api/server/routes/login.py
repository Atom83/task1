from server.app import sentry_sdk
from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.encoders import jsonable_encoder
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import SignatureExpired, BadSignature
from sqlalchemy.exc import SQLAlchemyError
from server.database import get_user_db
from server.models.user import ErrorResponseModel, UserSchema
from server.config import settings

router = APIRouter()

def verify_token(request: Request, token:str = None):
    if not token:
        token = request.headers.get("Authorization", "")
        action = ""
    else:
        action = " of action"
    s = Serializer(settings.TOKEN_KEY, settings.TOKEN_LT)
    verify_token.loads = []
    try:
        verify_token.loads = s.loads(token)
        sentry_sdk.capture_message(f"User's (ID={verify_token.loads['user_id']}) token accepted")
        return verify_token.loads
    except SignatureExpired:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Signature expired{action}")
    except BadSignature:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Bad signature{action}")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=e)

@router.post("/", response_description="Token retrieved")
async def login(user: UserSchema = Body(...)):
    user = jsonable_encoder(user)
    try:
        user_data = await get_user_db(user["login"])
    except SQLAlchemyError as e: 
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Internal server error", 500, e.orig.args)
    except Exception as e: 
        sentry_sdk.capture_exception(e)
        return ErrorResponseModel("Error", 520, e)
    
    err_msg = "Incorrect login or password"
    if user_data:
        if user_data["password"] == user["password"]:
            s = Serializer(settings.TOKEN_KEY, settings.TOKEN_LT)
            token = s.dumps({"user_id": str(user_data["_id"]), "login":user["login"]}).decode("utf-8")
            return {"code":200, "token":token}
    if err_msg:
        sentry_sdk.capture_message("Incorrect login or password")
        return ErrorResponseModel("Unauthorized", 401, err_msg)
