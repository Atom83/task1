from server.app import sentry_sdk
from fastapi import APIRouter, Body, Request
from fastapi.encoders import jsonable_encoder
from server.database import get_user_db, get_item_by_id_db
from server.models.user import ErrorResponseModel
from server.routes.login import verify_token
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from server.config import settings

router = APIRouter()

@router.post("/", response_description="Send URL gennerated")
async def send(request: Request, send_data = Body(...)):
    user_id_from = verify_token.loads["user_id"]
    send_data = jsonable_encoder(send_data)
    item_id = send_data["item_id"]
    login = send_data["login"]
    user = await get_user_db(login)
    if not user:
        sentry_sdk.capture_message(f"User {login}, to recieve an item, not found")
        return ErrorResponseModel("Not found", 404, "User not found")
    user = jsonable_encoder(user)
    item = await get_item_by_id_db(item_id)
    if not item:
        sentry_sdk.capture_message(f"Item {item_id}, to transfer to a user, not found")
        return ErrorResponseModel("Not found", 404, "Item not found")
    item = jsonable_encoder(item)
    url = request.url_for("get")
    s = Serializer(settings.TOKEN_KEY, settings.TOKEN_LT)
    token = s.dumps({"item_id":item_id, "user_id":user["_id"]}).decode("utf-8")
    sentry_sdk.capture_message(f"Send item {item_id} URL gennerated from user {user_id_from} to user {user['_id']}")
    return f"{url}?act={token}"